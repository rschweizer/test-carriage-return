print("Text followed by extra \\r (and then implicit \\n, which Python translates into \\r\\n on Windows)\r")
print("Text followed by two \\r\\r (To show what happens on Linux for double \\r\\r)\r\r")
print("Text without carriage return, followed by implicit newline (translated in to \\r\\n on Windows)")
